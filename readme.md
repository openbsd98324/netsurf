# netsurf

## Medias

![](medias/1646579280-screenshot.png)

![](medias/1646579295-screenshot.png)


![](medias/1692167676-screenshot.png)



## Linux Manjaro

````                           
 pacman -Fy   netsurf    

:: Synchronizing package databases...
 core is up to date
 extra                  40.6 MiB  3.02 MiB/s 00:13 [----------------------] 100%
 multilib is up to date
extra/netsurf 3.10-7
    usr/bin/netsurf

 pacman -S --noconfirm      netsurf 

Packages (9) libcss-0.9.1-6  libdom-0.4.1-2  libhubbub-0.3.7-2
             libnsbmp-0.1.6-8  libnsgif-0.2.1-8  libnsutils-0.1.0-2
             libparserutils-0.2.4-5  libwapcaplet-0.4.3-2  netsurf-3.10-7

Total Download Size:   1.52 MiB
Total Installed Size:  5.82 MiB

:: Proceed with installation? [Y/n] 
:: Retrieving packages...
 libhubbub-0.3.7-...    88.8 KiB   258 KiB/s 00:00 [----------------------] 100%
 libdom-0.4.1-2-x...   128.7 KiB   354 KiB/s 00:00 [----------------------] 100%
 libcss-0.9.1-6-x...   146.8 KiB   386 KiB/s 00:00 [----------------------] 100%
 libnsbmp-0.1.6-8...    11.8 KiB   129 KiB/s 00:00 [----------------------] 100%
 libparserutils-0...    38.0 KiB   328 KiB/s 00:00 [----------------------] 100%
 libnsgif-0.2.1-8...    10.8 KiB  96.7 KiB/s 00:00 [----------------------] 100%
 netsurf-3.10-7-x...  1115.0 KiB  1507 KiB/s 00:01 [----------------------] 100%
 libnsutils-0.1.0...     8.9 KiB   124 KiB/s 00:00 [----------------------] 100%
 libwapcaplet-0.4...     8.7 KiB  98.3 KiB/s 00:00 [----------------------] 100%
 Total (9/9)          1557.5 KiB  1811 KiB/s 00:01 [----------------------] 100%

````

# Options

````
OPTIONS

       This  programs  follow  the usual GNU command line syntax, with long options starting with
       two dashes (`-').

       The command line parameters override any options loaded from the users Choices file.

       A summary of options is included below.

       -v     Enable verbose logging.

       --http_proxy
              Bboolean indicating if the http proxy is being used.

       --http_proxy_host
              The http proxy host.

       --http_proxy_port
              The http proxy port.

       --http_proxy_auth
              The authentication scheme for the http proxy.

       --http_proxy_auth_user
              The authentication schemes user.

       --http_proxy_auth_pass
              The authentication schemes password.

       --font_size
              The default font size to use.

       --font_min_size
              The minimum font size to use.

       --font_sans
              Family name of the sans serrif font.

       --font_serif
              Family name of the serrif font.

       --font_mono
              Family name of the monospace font.

       --font_cursive
              Family name of the cursive font.

       --font_fantasy
              Family name of the fantasy font.

       --accept_language
              Languages to accept.

       --accept_charset
              Character set to accept

       --memory_cache_size
              Maximum memory cache size.

       --disc_cache_age
              Maximum disc cache size.

       --block_advertisements
              Boolean to enable ad blocking.

       --minimum_gif_delay
              Minimum time between gif frames

       --send_referer
              Boolean controlling wether referer data should be sent

       --animate_images
              Boolean controlling wether images should be animated.

       --expire_url
              expire url

       --font_default
              Default font.

       --ca_bundle
              ca bundle

       --ca_path
              ca path

       --cookie_file
              cookie file

       --cookie_jar
              cookie jar

       --homepage_url
              homepage url

       --search_url_bar
              search url bar

       --search_provider
              search provider

       --url_suggestion
              url suggestion

       --window_x
              The X co-ordinate of the initial window.

       --window_y
              The Y co-ordinate of the initial window.

       --window_width
              The width of the initial window.

       --window_height
              The height of the initial window.

       --window_screen_width
              window screen width

       --window_screen_height
              window screen height

       --toolbar_status_size
              toolbar status size

       --scale
              Initial scale factor.

       --incremental_reflow
              Boolean controlling wether incremental reflow is performed.

       --min_reflow_period
              Minimum time between incremental reflows

       --core_select_menu
              core select menu

       --max_fetchers
              max fetchers

       --max_fetchers_per_host
              max fetchers per host

       --max_cached_fetch_handles
              max cached fetch handles

       --suppress_curl_debug
              suppress curl debug

       --target_blank
              target blank

       --button_2_tab
              button 2 tab

       --margin_top
              margin top

       --margin_bottom
              margin bottom

       --margin_left
              margin left

       --margin_right
              margin right

       --export_scale
              export scale

       --suppress_images
              suppress images

       --remove_backgrounds
              remove backgrounds

       --enable_loosening
              enable loosening

       --enable_PDF_compression
              enable PDF compression

       --enable_PDF_password
              enable_PDF_password

       --gui_colour_bg_1
              gui colour bg_1

       --gui_colour_fg_1
              gui colour fg_1

       --gui_colour_fg_2
              gui colour fg_2

       --sys_colour_ActiveBorder
              Override CSS sys_colour_ActiveBorder colour.

       --sys_colour_ActiveCaption
              Override CSS sys_colour_ActiveCaption colour.

       --sys_colour_AppWorkspace
              Override CSS sys_colour_AppWorkspace colour.

       --sys_colour_Background
              Override CSS sys_colour_Background colour.

       --sys_colour_ButtonFace
              Override CSS sys_colour_ButtonFace colour.

       --sys_colour_ButtonHighlight
              Override CSS sys_colour_ButtonHighlight colour.

       --sys_colour_ButtonShadow
              Override CSS sys_colour_ButtonShadow colour.

       --sys_colour_ButtonText
              Override CSS sys_colour_ButtonText colour.

       --sys_colour_CaptionText
              Override CSS sys_colour_CaptionText colour.

       --sys_colour_GrayText
              Override CSS sys_colour_GrayText colour.

       --sys_colour_Highlight
              Override CSS sys_colour_Highlight colour.

       --sys_colour_HighlightText
              Override CSS sys_colour_HighlightText colour.

       --sys_colour_InactiveBorder
              Override CSS sys_colour_InactiveBorder colour.

       --sys_colour_InactiveCaption
              Override CSS sys_colour_InactiveCaption colour.

       --sys_colour_InactiveCaptionText
              Override CSS sys_colour_InactiveCaptionText colour.

       --sys_colour_InfoBackground
              Override CSS sys_colour_InfoBackground colour.

       --sys_colour_InfoText
              Override CSS sys_colour_InfoText colour.

       --sys_colour_Menu
              Override CSS sys_colour_Menu colour.

       --sys_colour_MenuText
              Override CSS sys_colour_MenuText colour.

       --sys_colour_Scrollbar
              Override CSS sys_colour_Scrollbar colour.

       --sys_colour_ThreeDDarkShadow
              Override CSS sys_colour_ThreeDDarkShadow colour.

       --sys_colour_ThreeDFace
              Override CSS sys_colour_ThreeDFace colour.

       --sys_colour_ThreeDHighlight
              Override CSS sys_colour_ThreeDHighlight colour.

       --sys_colour_ThreeDLightShadow
              Override CSS sys_colour_ThreeDLightShadow colour.

       --sys_colour_ThreeDShadow
              Override CSS sys_colour_ThreeDShadow colour.

       --sys_colour_Window
              Override CSS sys_colour_Window colour.

       --sys_colour_WindowFrame
              Override CSS sys_colour_WindowFrame colour.

       --sys_colour_WindowText
              Override CSS sys_colour_WindowText colour.

       --render_resample
              render resample

       --downloads_clear
              downloads clear

       --request_overwrite
              request overwrite

       --downloads_directory
              downloads directory

       --url_file
              url file

       --show_single_tab
              Force tabs to always be show.

       --button_type
              button type

       --disable_popups
              disable popups

       --disable_plugins
              disable plugins

       --history_age
              history age

       --hover_urls
              hover urls

       --focus_new
              focus new

       --new_blank
              new blank

       --hotlist_path
              hotlist path

       --source_tab
              source tab

       --current_theme
              current theme

````

## AUTHOR

       netsurf was written by <upstream author>.

       This manual page was written by Vincent Sanders <vince@debian.org>, for the Debian project
       (and may be used by others).



